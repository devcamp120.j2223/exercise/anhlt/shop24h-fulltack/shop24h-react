import Carousel from 'react-bootstrap/Carousel';
import anh1 from "../../../assets/image/1.jpg"
import anh2 from "../../../assets/image/2.jpg"
import anh3 from "../../../assets/image/3.jpg"
import anh4 from "../../../assets/image/4.jpg"

function CarouselShop() {
  return (
    <Carousel >
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={anh1}
          alt="First slide"
          sx={{height: "500px"}}
        />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={anh2}
          alt="Second slide"
          sx={{height: "500px"}}

        />

        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={anh3}
          alt="Third slide"
          sx={{height: "500px"}}
        />
        <Carousel.Caption>
        
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={anh4}
          alt="Third slide"
          sx={{height: "500px"}}
        />
        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default CarouselShop;