import { Card, CardContent, Stack, Rating, FormLabel, FormControl, LinearProgress, Typography,RadioGroup, FormGroup,Radio, FormControlLabel, Checkbox, Paper, InputBase, Pagination, Grid } from '@mui/material';
import { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from "reactstrap";
import { useDispatch, useSelector } from "react-redux"

function FilterProduct() {
    const dispatch = useDispatch();
    const { nameInput, minInput, maxInput, type } = useSelector((reduxData) => reduxData.taskReducer);

    const inpNameChang = (event) => {
        dispatch({
            type: "VALUE_NAME",
            payload: {
                nameInput: event.target.value
            }
        })

    }
    const inpMimOnchange = (event) => {
        dispatch({
            type: "VALUE_MIN",
            payload: {
                minInput: event.target.value
            }
        })
    }
    const inpMaxOnchange = (event) => {
        dispatch({
            type: "VALUE_MAX",
            payload: {
                maxInput: event.target.value
            }
        })
    }
    const onChangeRadio = (event) =>{
        console.log(event.target.value)
        dispatch({
            type: "VALUE_RADIO",
            payload: {
                type: event.target.value
            }
        })
    }


    return (
        <>
            <Col className="col-3 p-5" mt={3} >
                <Card sx={{ width: 200, boxShadow: "none" }}>
                    <CardContent>
                        <Typography sx={{ fontSize: 20 }} gutterBottom  >
                            Tìm Sản phẩm
                        </Typography>
                        <Typography sx={{ fontSize: 15, color: "black" }} gutterBottom mt={2}  >
                            Tên Đồng hồ
                        </Typography>
                        <Typography variant="h5" component="div">
                            <Paper
                                component="form"
                                sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 170 }}
                            >
                                <InputBase
                                    placeholder="Tên" onChange={inpNameChang} value={nameInput}
                                />
                            </Paper>
                        </Typography>
                        <Typography sx={{ fontSize: 15, color: "black" }} gutterBottom mt={2} >
                            Giá sản phẩm
                        </Typography>
                        <Typography variant="h5" component="div">
                            <Paper
                                component="form"
                                sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 170 }}
                            >
                                <InputBase
                                    placeholder="Min"
                                    onChange={inpMimOnchange}
                                    value={minInput}
                                    sx={{width: 80}}

                                />
                                <InputBase
                                    placeholder="Max"
                                    onChange={inpMaxOnchange}
                                    value={maxInput}
                                    sx={{width: 80}}
                                />
                            </Paper>
                        </Typography>
                        <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                            Thể Loại
                        </Typography>
                        <Typography variant="h5" component="div">
                            <FormControl>
                                <FormLabel id="demo-radio-buttons-group-label"></FormLabel>
                                <RadioGroup
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    name="radio-buttons-group"
                                    defaultValue=""
                                    onChange={onChangeRadio}
                                >
                                    <FormControlLabel value="" control={<Radio />} label="Tất cả" />
                                    <FormControlLabel value="62bd0d038959b877d8cc5608" control={<Radio />} label="Đồng hồ cơ" />
                                    <FormControlLabel value="62bd266936e3529d1de6748b" control={<Radio />} label="Đồng hồ điện" />
                                    
                                </RadioGroup>
                            </FormControl>
                        </Typography>
                        <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                            Suất sứ
                        </Typography>
                        <Typography variant="h5" component="div">
                            <FormGroup>
                                <FormControlLabel control={<Checkbox defaultChecked />} label="Thủy sĩ" />
                                <FormControlLabel control={<Checkbox defaultChecked />} label="Nhật Bản" />
                                <FormControlLabel control={<Checkbox defaultChecked />} label="Trung Quốc" />
                                <FormControlLabel control={<Checkbox defaultChecked />} label="Việt Nam" />

                            </FormGroup>
                        </Typography>
                        <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                            Rating
                        </Typography>
                        <Typography variant="h5" component="div">
                            <Stack spacing={1 }>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={0} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={1} readOnly sx={{ alignItems: "center"}} />
                                </Grid>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={2} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={3} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={4} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                                <Grid container>
                                    <FormControlLabel control={<Checkbox defaultChecked />} />
                                    <Rating name="half-rating" defaultValue={5} readOnly sx={{ alignItems: "center" }} />
                                </Grid>
                            </Stack>
                        </Typography>
                    </CardContent>
                </Card>
            </Col>
        </>
    )
}
export default FilterProduct